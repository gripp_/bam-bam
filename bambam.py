import itertools
import os
import pickle
import random
import string

from numpy import array
from tensorflow.keras import Sequential
from tensorflow.keras.layers import Dense, Embedding, LSTM
from tensorflow.keras.models import load_model
from tensorflow.keras.preprocessing.sequence import pad_sequences
from tensorflow.keras.preprocessing.text import Tokenizer
from tensorflow.keras.utils import to_categorical

def _clean_line(line):
  clean_line = line.lower()

  clean_line = ''.join(
      [
        (
          c
          if c in (
            string.ascii_lowercase +
            string.digits +
            string.whitespace +
            '.')
          else '')
        for c
        in clean_line
      ])
  clean_line = ' '.join(clean_line.split())

  return clean_line

class Brain(object):
  BOOK_DIR = 'texts/'
  BOOKS = os.listdir(BOOK_DIR)
  NAME = 'bambam'
  SENTENCE_LENGTH = 'sentencelength'
  SEQUENCE_LENGTH = 'sequencelength'
  TEXTS = 'texts'
  TOKENIZER = 'tokenizer'
  VOCAB_SIZE = 'vocabsize'

  def __init__(self, info=None, model=None):
    self._library = set()
    self._model = model
    self._sentence_length = (
        info[Brain.SENTENCE_LENGTH]
        if info is not None and Brain.SENTENCE_LENGTH in info
        else -1)
    self._texts = (
        info[Brain.TEXTS]
        if info is not None and Brain.TEXTS in info
        else [])
    self._tokenizer = (
        info[Brain.TOKENIZER]
        if info is not None and Brain.TOKENIZER in info
        else Tokenizer(
            filters='!"#$%&()*+,-/:;<=>?@[\\]^_`{|}~\t\n', lower=True))
    self._vocab_size = (
        info[Brain.VOCAB_SIZE]
        if info is not None and Brain.VOCAB_SIZE in info
        else -1)

   def think(self):
     while True:
       self._read_book()
       self._consider()

       self._converse()
       self._consider()

  def _consider(self):
    tokens = list(
        itertools.chain.from_iterable([ line.split() for line in self._texts ]))

    lines = []
    for i in range(self._sentence_length, len(tokens)):
      seq = tokens[i - self._sentence_length : i]
      line = ' '.join(seq)
      lines.append(line)

    self._tokenizer.fit_on_texts(lines)
    sequences = self._tokenizer.texts_to_sequences(lines)

    vocab_size = len(self._tokenizer.word_index) + 1

    sequences = array(sequences)
    exx, wyy = sequences[:,:-1], sequences[:,-1]
    wyy = to_categorical(wyy, num_classes=vocab_size)

    if vocab_size > self._vocab_size:
      self._vocab_size = vocab_size
      self._vocab_size = vocab_size

      self._model = Sequential()
      self._model.add(
          Embedding(
              self._vocab_size,
              self._sentence_length - 1,
              input_length=exx.shape[1]))
      self._model.add(LSTM(100, return_sequences=True))
      self._model.add(LSTM(100))
      self._model.add(Dense(100, activation='relu'))
      self._model.add(Dense(vocab_size, activation='softmax'))
      self._model.compile(
          loss='categorical_crossentropy',
          optimizer='adam',
          metrics=['accuracy'])

    self._model.fit(exx, wyy, batch_size=128, epochs=100)

    self._model.save(f'{Brain.NAME}-model.h5')
    pickle.dump(self._get_info(), open(f'{Brain.NAME}-info.pkl', 'wb'))

  def _converse(self):
    convo = [ 'hello .' ]
    print(f'> {convo[0]}')

    msg = None
    while msg != 'bye.':
      msg = input(':: ')
      msg = msg.lower()

      words = msg.split()

      if len(words) == 0:
        continue

      if len(words) > 1 and words[0] == 'say':
        response = ' '.join(words[1:])
      else:
        convo.append(_clean_line(msg))
        response = self._respond(_clean_line(msg))

      convo.append(response)
      print(f'> {response}')

    convo.append('goodbye .')
    print(f'> {convo[-1]}')

    print(f'reading conversation')
    return self._read(convo)

  def _get_info(self):
    return {
      Brain.SENTENCE_LENGTH: self._sentence_length,
      Brain.TEXTS: self._texts,
      Brain.TOKENIZER: self._tokenizer,
      Brain.VOCAB_SIZE: self._vocab_size,
    }

  def _read_book(self):
    if len(self._library) == len(Brain.BOOKS):
      return False

    next_book = random.choice(Brain.BOOKS)

    while next_book in self._library:
      next_book = random.choice(Brain.BOOKS)

    self._library.add(next_book)

    with open(f'{Brain.BOOK_DIR}{next_book}', 'r') as book_file:
      book_lines = book_file.readlines()

    first_sentence = (
        _clean_line(book_lines[0]) if len(book_lines) > 0 else None)
    first_sentence = (
        list(
            itertools.chain.from_iterable(
              [ l.split() + ['.'] for l in first_sentence.split('.') ]))[0]
        if first_sentence is not None
        else None)

    if first_sentence is not None:
      print(f'reading {next_book}')
      self._read(book_lines)

    return len(self._library) < len(Brain.BOOKS)

  def _read(self, lines):
    lines = [
      _clean_line(line)
      for line
      in lines
      if line.find('.') >= 0 and not line[0].isdigit()
    ]

    sentences = list(itertools.chain.from_iterable([
      [ l.split() + ['.'] for l in line.split('.') ]
      for line
      in lines
    ]))

    self._texts += [
        ' '.join(s)
        for s
        in sentences
        if len(s) > 1
    ]

    self._sentence_length = (
        self._sentence_length
        if self._sentence_length > 0
        else len(sorted(sentences, key=lambda s: len(s), reverse=True)[0]))

  def _respond(self, msg):
    def _word_in_vocab(word):
      return word in self._tokenizer.word_index.keys()

    txt = ' '.join(
        [
          word
          for word
          in msg.split()
          if _word_in_vocab(word)
        ])
    next_word = ''
    response = ''

    while next_word != '.':
      encoded = self._tokenizer.texts_to_sequences([txt])[0]
      encoded = pad_sequences(
          [encoded], maxlen=self._sentence_length, truncating='pre')
      yhat = self._model.predict_classes(encoded, verbose=0)

      next_word = ''
      for word, index in self._tokenizer.word_index.items():
        if index == yhat:
          next_word = word

      response = (
          ' '.join([response, next_word]) if len(response) > 0 else next_word)
      txt = response

    return response

if __name__ == '__main__':
  model_file = f'{Brain.NAME}-model.h5'
  info_file = f'{Brain.NAME}-info.pkl'

  model = load_model(model_file) if os.path.isfile(model_file) else None
  info = (
      pickle.load(open(info_file, 'rb'))
      if os.path.isfile(info_file)
      else None)

  soul = Brain(info=info, model=model)
  soul.think()
