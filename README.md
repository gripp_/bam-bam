# BAMBAM

bambam is a chatbot built on a tensorflow neural net. ey parse books and converse and uses those texts to train eir brain, which is written in vanilla python3 and thus should be runnable pretty much anywhere. but here are instructions for getting em running on mac os x:

1. open the terminal (`⌘-[space]` then type `terminal`).
2. make sure python3 is installed (`brew install python3`).
3. make sure git is installed (`brew install git`).
4. clone the bambam repo (`git clone https://gripp_@bitbucket.org/gripp_/bam-bam.git ~/bam-bam`).
5. navigate to your new repository (`cd ~/bam-bam`) and check that everything is there (`ls`).
6. recommended but not strictly speaking required: create and launch a virtual environment (`pip3 install virtualenv` then `virtualenv env` then `
source env/bin/activate`).
7. install bambams requirements (`pip3 install -r requirements.txt`).
8. launch bambam (`python3 bambam.py`).

bambam will alternate between reading a book from the texts directory and conversing in the terminal. the model it generates will be saved alongside the code. **training the model takes a long time! probably several hours!** when the model is done training, youll see bambam say `> hello .` and then youll be prompted with `:: `, which is your cue to chat! talk as much as you like, and when youre ready to end the conversation say `bye.` (with the period). when you do, bambam will retrain with the conversation you had (again possibly taking a few hours) and pick another book (hours again).

- you can end the program by hitting `^-C`. the model saves after it finishes training, so if you end the program during a conversation bambam will remember everything through the previous book. if you want to wipe bambambs brain and start fresh, delete the h5 and pkl files (`rm \*.h5 \*.pkl`).
- when youre conversing with bambam, you can say `say [phrase]` to make bambam repeat a phrase. i figured that could be useful for targeting specific phrases / behavior.
- running bambams brain will cause a lot of debugger output, primarily from the neural net library. this happens both during training and conversation. you can mostly just ignore it. phrases that bambam is trying to say are prefaced with `>`.
- because this code is new and brittle, its likely you will encounter bugs and errors. let me (marshall@glasseyeballs.com) know!
